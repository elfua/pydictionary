# -*- coding: cp1251 -*-

"""
Configration settings for pyDSL2HTM
"""
import os, re


# Separators for entries and fields (used for database importing)
ENTRY_SEPARATOR = os.linesep + '<!--~-->'
# FIELD_SEPARATOR = os.linesep + '<!--=-->'
FIELD_SEPARATOR = '<!--=-->'

# Separators for entries and fields (used for pretty HTML file mode)
ADDITIONAL_ENTRY_SEPARATOR_PRETTY = '<DT>'
ADDITIONAL_FIELD_SEPARATOR_PRETTY = ''

# OLD::: PATTERN_ENTRY = re.compile(END_OF_LINE+'(?=\S)', re.UNICODE | re.IGNORECASE |re.MULTILINE)
# BAD ENTRY_MATCH_PATTERN = re.compile(r'[\r\n]+(?![\t ])', re.UNICODE)

# Expressions for matching (1) the entry start, and (2) head/definition separator
ENTRY_MATCH_PATTERN = re.compile(r'(?:\r|\n)+(?=[\w~�-�@\-\'\"\.])', re.UNICODE | re.IGNORECASE)
FIELD_MATCH_PATTERN = re.compile(r'(?:\r|\n)+', re.UNICODE )

# Strings to replace on first pass
DSL2HTM_DICT_1ST_PASS = {
'<<':'[ref]',
'>>':'[/ref]',
}

DSL2HTM_DICT_2ND_PASS = {
'>':'&gt;',
'<':'&lt;'
}


# Strings to replace on second pass
DSL2HTM_DICT = {
    # opening tags to SPANs
    '\[(m|c|trn|t|com|p|ex|ref)\]':'<span class="\\1">',
    '\[\*\]':'<span class="star">',
    '\[\'\]':'<span class="stress">',
    '(\[ref\s[^\]]+\]|<<)':'<span class="ref">',
    '>>':'</span>',

    # closing the SPANs
    '\[/(?:c|m|trn|t|com|p|ex|ref|\'|\*|m)\]':'</span>',

    #COLOR
    '\[c ([\w]+)\]':'<span class="col_\\1">',

    # TO SELF
    '\[(sup|sub|i|b)\]':'<\\1>',
    '\[/(sup|sub|i|b)\]':'</\\1>',

    # Indentation (m1, m2, m3, m4...)
    '\[m(\d)\]':'<span class="m\\1">',


    # EMPTY TAGS (replace with nothing)
    '\[/?(?:trn|lang)\]':'',
    '\[lang\sid=\d+\]':'',
    
    # ESCAPES and white space (strip)
    r'\\':'',
    '\t':'',
}

# HTML and CSS for pretty HTML output
HTML_HEADER = r'''<HTML>
<HEAD>
 <TITLE></TITLE>
 <META HTTP-EQUIV=Content-Type CONTENT="text/html; charset=%s" />
 <style type="text/css">
     body {padding: 1px}
      p, body, table {font-family: "Arial Unicode MS", verdana, arial; font-size: 9pt;}
      table { border: 1px solid #cdc1ae; border-collapse: collapse; padding: 0px; margin: 1px;}
      TH, {border: 1px solid #cdc1ae;  }
      DT {text-align: left; border: 1px solid white; border-bottom: 1px solid #d5cdbf; border-right: 1px solid #e5dfce; color: #817e70;margin: 2px 80px ; padding: 5px 10px; background: #eff4e5; font-weight: bold}
      DD {text-align: left; margin: 0px 80px ; padding: 0px 110px; background: #f9f9fb; vertical-align: }
      dd p {margin: 0px; padding: 1px}
      TD {border: 1px solid #cdc1ae; padding: 2px;vertical-align: top;}
      TR { padding: 2px;background:#fbf8e9 ; }
      h1 {font-size: 13pt; color: #999999}
      pre {font-family: "Lucida Console", "Lucida Sans Unicode"}
      A:link{Color : #00008B; Font-Family : Arial, Helvetica, sans-serif;Text-Decoration : none; }
      A:visited{Color : #00008B;Font-Family : Arial, Helvetica, sans-serif;Text-Decoration : none;}
      A:hover{Color : #A0522D;Font-Family : Arial, Helvetica, sans-serif;Text-Decoration : none;}
      table.search {border: 0px; background: #e8e7d8 }
      table.search td {text-align: right; border: 0px}
      .info {color: #bbbbbb; padding: 3px}
      .even {background-color: #f5f0df}
      input {border:1px solid #a3a3a3;background: #fffbd5; margin: 5px}
      select {background-color: #f9f2f2; color: Olive; border:1px solid #d6d1c0;}
      .button {border:1px solid #d6d1c0; background: #e8e7d8 }
      .dbg {color:#eaebec} 
      .prev{display: inline; float: left; text-align:left; width: 100px; }
      .next{display: inline; float: right; text-align: right; width: 100px; }
      .star {color:#395666}
      .ref{font-weight: bold; color: #009999}
      .m1{margin-left: 0px;}
      .m2{margin-left: 5px;}
      .m3{margin-left: 10px;}
      .m4{margin-left: 14px;}
      .m1{margin-left: 0px;}
      .m2{margin-left: 5px;}
      .m3{margin-left: 10px;}
      .m4{margin-left: 14px;}
      .m2 b i {color: #996300}
      .m3 b {color: darkblue}
      .p1 {color: #333b14}
      .col1{color: #660066}
      .col2{color: #006600}
      .col3{color: #857d49}
      .col4 {color: gray}
      .m3 .col4{color: darkgreen}
      .ex {color: #000099; font-family: "Trebuchet MS", "Times New Roman"}
      .com{color: darkred}
 </style>
</HEAD>
<BODY BGCOLOR="#FFFFFF" TET="#000000" LINK="#000099" VLINK="#330066" ALINK="#FF0000">
<DL>
'''

HTML_FOOTER =r'''
</DL>
</BODY>
</HTML>
'''
